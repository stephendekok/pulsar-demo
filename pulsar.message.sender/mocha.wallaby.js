
module.exports = function (wallaby) {

    return {
        files: [
            'src/**/*.+(js|jsx|json|snap|css|less|sass|scss|jpg|jpeg|gif|png|svg)',
            {pattern: '**/*.js', instrument: false},
            "docker-compose.yml",
            "*.env",
            '!**/*.test.js',
            "!node_modules/**/*"
        ],

        tests: ['src/**/*.test.js?(x)', 'src/*.test.js?(x)'],

        env: {
            type: 'node',
            runner: 'node'
        },

        setup: wallaby => {
            process.env.DATABASE = "localhost"
            process.env.DATABASE_PORT = "3307"
            process.env.DATABASE_USERNAME = "root"
            process.env.DATABASE_PASSWORD = ""
            process.env.REDIS = "redis://127.0.0.1:6379"
        },
        teardown: () => {
        },

        testFramework: 'mocha'
    };
};
