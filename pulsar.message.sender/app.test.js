global.regeneratorRuntime = require('regenerator-runtime/runtime');
require('dotenv').config();
const chai = require('chai');
// const sinon = require('sinon');
chai.use(require('chai-as-promised'));
const {expect} = chai;
// const {Database: {query}, MutationRelay} = require('alcumus-data-source-server');
// MutationRelay.enable();
const app = require('./app');
const request = require('supertest');
const uuid = require('uuid');

describe("app", function(){
    this.timeout(1000*3600); //1hr
    it('throughput test', async function () {
        //let fn = sinon.stub();
        //processor('test', fn);
        for (let i = 0; i < 1000000; i++) {
            console.log(i);
            await request(app)
                .post('/enqueue')
                .send([{type: 'test', name:`name: ${i}`}])
                .set('Content-Type', 'application/json')
                .set('Accept', 'application/json')
                .expect(200);
            }
        }
    )

        it('should be able to call process functions', async function () {


            await request(app)
                .post(`/enqueue?client=${uuid()}&token=magic`)
                .send([{type: 'user.get',
                    $id: uuid()}])
                .set('Content-Type', 'application/json')
                .set('Accept', 'application/json')
                // .set('x-access-token', 'magic')
                .expect(200);


            // await request(app)
            //     .post('/enqueue')
            //     .send([{type: 'test.users.get2', name:`name: test2`}])
            //     .set('Content-Type', 'application/json')
            //     .set('Accept', 'application/json')
            //     .expect(200);
            }
        )
}
)