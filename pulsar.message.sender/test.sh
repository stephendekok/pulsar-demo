#!/usr/bin/env bash
docker-compose up -d
sleep 1
docker-compose exec mysql mysql -e "alter user 'root'@'localhost' identified with mysql_native_password by '';alter user 'root'@'%' identified with mysql_native_password by '';"
./node_modules/mocha/bin/mocha --exit "./{,!(node_modules)**/}/src/**/*.test.js"
docker-compose down
