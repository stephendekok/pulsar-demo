global.regeneratorRuntime = require('regenerator-runtime/runtime');
require('dotenv').config();
const {processor, queue} = require('alcumus-processor');

(async() => {
    const logger = await processor('user.get', function (data) {
        console.log(data);
    });

    const logger2 = await processor('test.users.get2', function (data) {
        console.log(data);
    });
})()